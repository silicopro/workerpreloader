# Getting Started

WebWorkerを使った簡易プリロードクラス。
WebWorker使えなかったら普通にxhrでロード。

コンソラクタの引数には以下のようなObjectを渡す

```
{
    // CDNのURL
    cdnBasePath: "Config.cdnBasePath"

    // maxConnection
    maxConnection: 6

    // 画像パス
    src: [
      "img/1.png"
      "img/2.png"
      "img/3.png"
    ]
}
```
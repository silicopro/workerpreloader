
@onmessage = (_data)->
  @.data = _data.data
  @.url = @.data.src
  @.cdnBasePath = @.data.cdnBasePath
  @.loadingCnt = 0
  @.loadedCnt = 0
  @.itemLen = @.url.length

  load()


@load = ->

#  postMessage(status:"progress", param:@.loadedCnt/@.itemLen)
  @post "progress", @.loadedCnt/@.itemLen

  if @.url.length>0
    cnt = @.data.maxConnection-@.loadingCnt
    for i in [0..cnt-1]
      if @.url.length>0
        target = @.url.shift()
        loadWithWorker target, load
  else
    if @.loadedCnt == @.itemLen
      @post "complete"



@loadWithWorker = (url, callback)->
  xhr = new XMLHttpRequest()
  @.loadingCnt++
  xhr.open("GET", @.cdnBasePath+url, true)
  xhr.onreadystatechange = (data)=>
    if xhr.readyState is 4
      if xhr.status is 200
        @.loadingCnt--
        @.loadedCnt++
        callback()
      else
        @post "error"
      return

  xhr.onerror = (data)=>
    @post "error"
    return

  xhr.send(null)

  return

@post = (status, param)->
  postMessage(status: status, param: param)



class PreloadController

  WORKER_URL: 'js/include/WorkerPreloader.js'

  data:
    cdnBasePath: ""
    src: []
    maxConnection: 6

  onProgress: null

  onComplete: null

  onError: null

  ###
  Preload Class
  ###
  constructor: (_data) ->

    # スマートフォンのMaxConnectionの設定
    ua = window.navigator.userAgent.toLowerCase()
    if ua.indexOf("iphone") > 0 or ua.indexOf("ipad") > 0 or ua.indexOf("ipod") > 0 or ua.indexOf("android") > 0
      @data.maxConnection = 4

    # OPTIONS
    if _data.cdnBasePath?
      @data.cdnBasePath = _data.cdnBasePath

    if _data.src?
      @data.src = _data.src

    if _data.maxConnection?
      @data.maxConnection = _data.maxConnection

    return



  ###
  ロード開始
  ###
  start: =>

    @enableWorker = !!window.Worker

    # Detect Loader
    if @enableWorker
      @workerloadStart()
    else
      @xhrloadStart()
    return


  ###
  worker版ロード開始
  ###
  workerloadStart: =>
    @worker = new Worker(@data.cdnBasePath+@WORKER_URL)

    @worker.onmessage = (work)=>
      if work.data.status is 'complete'
        if @onComplete
          @onComplete()
      else if work.data.status is 'progress'
        if @onProgress
          @onProgress(work.data.param)
      else if work.data.status is 'error'
        if @onError
          @onError()

      return

    @worker.postMessage(@data)

    return

  ###
  非worker版ロード開始
  ###
  xhrloadStart: =>
    @url = @data.src
    @itemLen = @url.length
    @loadingCnt = 0
    @loadedCnt = 0
    @load()
    return


  load: =>

    if @onProgress
      @onProgress @loadedCnt/@itemLen

    if @url.length>0

      cnt = @data.maxConnection-@loadingCnt
      for i in [0..cnt-1]
        if @url.length>0
          target = @url.shift()
          @loadWithXHR target, @load
    else
      if @loadedCnt == @itemLen
        if @onComplete
          @onComplete()


  loadWithXHR: (url, callback) =>
    xhr = new XMLHttpRequest()
    @loadingCnt++
    xhr.open("GET", @data.cdnBasePath+url, true)
    xhr.onload = (data)=>
      if xhr.readyState is 4
        if xhr.status is 200
          @loadingCnt--
          @loadedCnt++
          callback()
        else
          if @onError
            @onError()
        return

    xhr.onerror = (data)=>
      if @onError
        @onError()
      return

    xhr.send(null)

    return



module.exports = PreloadController
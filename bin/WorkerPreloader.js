this.onmessage = function(_data) {
  this.data = _data.data;
  this.url = this.data.src;
  this.cdnBasePath = this.data.cdnBasePath;
  this.loadingCnt = 0;
  this.loadedCnt = 0;
  this.itemLen = this.url.length;
  return load();
};

this.load = function() {
  var cnt, i, j, ref, results, target;
  this.post("progress", this.loadedCnt / this.itemLen);
  if (this.url.length > 0) {
    cnt = this.data.maxConnection - this.loadingCnt;
    results = [];
    for (i = j = 0, ref = cnt - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
      if (this.url.length > 0) {
        target = this.url.shift();
        results.push(loadWithWorker(target, load));
      } else {
        results.push(void 0);
      }
    }
    return results;
  } else {
    if (this.loadedCnt === this.itemLen) {
      return this.post("complete");
    }
  }
};

this.loadWithWorker = function(url, callback) {
  var xhr;
  xhr = new XMLHttpRequest();
  this.loadingCnt++;
  xhr.open("GET", this.cdnBasePath + url, true);
  xhr.onreadystatechange = (function(_this) {
    return function(data) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          _this.loadingCnt--;
          _this.loadedCnt++;
          callback();
        } else {
          _this.post("error");
        }
      }
    };
  })(this);
  xhr.onerror = (function(_this) {
    return function(data) {
      _this.post("error");
    };
  })(this);
  xhr.send(null);
};

this.post = function(status, param) {
  return postMessage({
    status: status,
    param: param
  });
};
